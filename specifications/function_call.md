```mermaid
graph TD
    A[initializeVideoPlayer] --> B[fetchMarkdownData_db_md]
    A --> C[fetchMarkdownData_keymap_md]
    B --> D[parseMarkdown_db_md]
    C --> E[parseMarkdown_keymap_md]
    D --> F[mergeDatabase]
    E --> F
    F --> G[renderVideoLinks]
    G --> H[createLinkForEachVideo]
    H --> I[setupEventListeners]
    I --> J[handleRFIDInput]
    I --> K[handleKeyboardInput]
    I --> L[handleDebugMode]
    I --> M[setupStandbyMode]
    M --> N[startBackgroundVideo]
    J --> O[playVideo]
    K --> O
    L --> O
    O --> P[fadeOut]
    P --> Q[videoPlayerPlay]
    Q --> R[fadeIn]

```