# Technical Documentation for Interactive Installation in Kiosk Mode

## System Overview

This interactive installation features an RFID-based video player operating in kiosk mode. It includes the following components:

- **USB RFID Reader**: Scans for keytags to trigger video playback.
- **Interactive Video Player**: Displays videos based on input from the RFID reader, keyboard, or touchscreen in debug mode.
- **Keytag Mapping**: Defined in a markdown document to map RFID keytags to corresponding videos.

## System Initialization

1. **Fullscreen Mode**: Upon system startup, the video player enters fullscreen mode.
2. **Background Video Loop**: The system loads and starts looping a configurable background video, used as the standby state video.

## Operation Modes

### Normal Usage

- **USB RFID Input**:
  - The system scans for RFID keytag input.
  - On keytag detection, it searches for a matching keytag in the database.
  - If a match is found, the corresponding video plays.
  - If no match is found, the system remains in the standby state with the background video loop.

- **Keyboard Input**:
  - The system reads a sequence of characters followed by the Enter key.
  - It searches for a matching keytag in the database.
  - If a match is found, the corresponding video plays.
  - If no match is found, the system remains in the standby state with the background video loop.

### Debug Mode

- **Index Input via Keyboard**:
  - The system reads a sequence of characters followed by the Enter key.
  - It searches for a matching ID in the database.
  - If a match is found, the corresponding video plays.
  - If no match is found, the system remains in the standby state with the background video loop.

- **Touchscreen Input**:
  - When debug mode is toggled ON, buttons for each video index are displayed.
  - Each button, labeled with the corresponding video index, triggers playback of the associated video when pressed.

### Planned Features

- **MIDI Input**: Integration for MIDI devices to trigger video playback.
- **WebSocket Input**: Support for WebSocket communication to control video playback.

## Database Merging

### Loading Database

- At runtime, the system attempts to load the database from a query operator in the URL.
- If the URL query is unavailable, the system loads the default configuration.

### Parsing Markdown Files

- **db.md**: Contains video metadata such as ID, label, and video file links.
- **keymap.md**: Maps RFID keytags to video IDs.

#### Example Structure of db.md

```
## 01
### id
* 01
### label
* sentience
### video
* [![01-sentience](thumb_01-sentience.webp)](01-sentience.mp4)

## 03
### id
* 03
### label
* waves
### video
* [![03-waves](thumb_03-waves.webp)](03-waves.mp4)
```

#### Example Structure of keymap.md

```
## 01
### keytag
* 0000010000
* 1000000000

## 03
### keytag
* 0000000003
* 3000000000
```

### Merging Process

- The system parses the markdown files into JSON objects.
- It loads all video-associated data, including ID, label, caption, videos, keytags, etc.
- Keymap elements from `keymap.md` are added to the corresponding IDs in the database.
- Example merged entry:

```
## 01
### id
* 01
### label
* sentience
### video
* [![01-sentience](thumb_01-sentience.webp)](01-sentience.mp4)
### keytag
* 0000000001
* 0000010000
* 1000000000
```

## Conclusion

This documentation outlines the setup and operational details of the interactive installation in kiosk mode. The system relies on markdown files for database configuration and supports multiple input methods for triggering video playback, including RFID, keyboard, and touchscreen inputs. Future enhancements include MIDI and WebSocket inputs.