export async function fetchMarkdownData(filePath) {
    const response = await fetch(filePath);
    const text = await response.text();
    return parseMarkdown(text);
  }
  
  export function parseMarkdown(markdown) {
    const entries = [];
    const sections = markdown.split('##').slice(1);
    sections.forEach(section => {
      const lines = section.split('\n').map(line => line.trim()).filter(line => line);
      const entry = {};
      lines.forEach(line => {
        if (line.startsWith('### id')) {
          entry.id = line.split('* ')[1];
        } else if (line.startsWith('### label')) {
          entry.label = line.split('* ')[1];
        } else if (line.startsWith('### video')) {
          entry.video = line.match(/\(([^)]+)\)/)[1];
        } else if (line.startsWith('### keytag')) {
          entry.keytags = (entry.keytags || []).concat(line.split('* ').slice(1));
        }
      });
      entries.push(entry);
    });
    return entries;
  }
  
  export function mergeDatabase(db, keymap) {
    keymap.forEach(entry => {
      const dbEntry = db.find(item => item.id === entry.id);
      if (dbEntry) {
        dbEntry.keytags = (dbEntry.keytags || []).concat(entry.keytags);
      }
    });
    return db;
  }
  
  export function logDatabase(database) {
    console.log('Database:', database);
  }
  