import { fetchMarkdownData, parseMarkdown, mergeDatabase, logDatabase } from './database.js';

let audioContext, masterGainNode;
let videoPlayers = {};
let gainNodes = {};
let currentPlaying = null;
let loggingEnabled = true;  // Set this to false to disable logging

export async function initializeVideoPlayer() {
  const mediaFolder = '../../medias/cyberdelia/';
  const db = await fetchMarkdownData(mediaFolder + 'db.md');
  const keymap = await fetchMarkdownData(mediaFolder + 'keymap.md');
  const database = mergeDatabase(db, keymap);

  if (loggingEnabled) {
    logDatabase(database);
  }

  setupAudioContext();
  await loadAllVideos(database, mediaFolder);
  setupEventListeners(database, mediaFolder);
  playBackgroundVideo(mediaFolder + 'background_video.mp4');
}

function setupAudioContext() {
  if (!audioContext) {
    audioContext = new (window.AudioContext || window.webkitAudioContext)();
    masterGainNode = audioContext.createGain();
    masterGainNode.connect(audioContext.destination);
  }
}

async function loadAllVideos(database, mediaFolder) {
  const container = document.getElementById('videoLinksContainer');
  for (const entry of database) {
    const videoElement = document.createElement('video');
    videoElement.src = mediaFolder + entry.video;
    videoElement.preload = 'auto';
    videoElement.style.display = 'none';
    document.body.appendChild(videoElement);

    const sourceNode = audioContext.createMediaElementSource(videoElement);
    const gainNode = audioContext.createGain();
    sourceNode.connect(gainNode).connect(masterGainNode);

    videoPlayers[entry.id] = videoElement;
    gainNodes[entry.id] = gainNode;

    const link = document.createElement('a');
    link.href = '#';
    link.innerText = entry.label;
    link.addEventListener('click', (e) => {
      e.preventDefault();
      playVideo(entry.id);
    });
    container.appendChild(link);
  }
}

function setupEventListeners(database, mediaFolder) {
  let inputSequence = '';
  document.addEventListener('keydown', async (event) => {
    if (event.key === 'Enter') {
      const videoId = findVideoByKeytag(database, inputSequence);
      if (videoId) {
        playVideo(videoId);
      }
      inputSequence = ''; // Reset input sequence after processing
    } else {
      inputSequence += event.key;
    }
  });

  document.getElementById('debugModeToggle').addEventListener('click', () => {
    const debugButtons = document.getElementById('debugButtons');
    debugButtons.style.display = debugButtons.style.display === 'none' ? 'flex' : 'none';
    database.forEach(entry => {
      const button = document.createElement('button');
      button.innerText = entry.id;
      button.addEventListener('click', () => playVideo(entry.id));
      debugButtons.appendChild(button);
    });
  });

  // Ensure the AudioContext is resumed on user interaction
  document.body.addEventListener('click', () => {
    if (audioContext.state === 'suspended') {
      audioContext.resume();
    }
  });
}

function playBackgroundVideo(backgroundVideoSrc) {
  const backgroundPlayer = document.createElement('video');
  backgroundPlayer.id = 'backgroundPlayer';
  backgroundPlayer.src = backgroundVideoSrc;
  backgroundPlayer.loop = true;
  backgroundPlayer.preload = 'auto';
  backgroundPlayer.muted = true; // Ensure autoplay is allowed by muting the video
  backgroundPlayer.style.display = 'none';
  document.body.appendChild(backgroundPlayer);

  const sourceNode = audioContext.createMediaElementSource(backgroundPlayer);
  const gainNode = audioContext.createGain();
  sourceNode.connect(gainNode).connect(masterGainNode);

  videoPlayers['background'] = backgroundPlayer;
  gainNodes['background'] = gainNode;

  backgroundPlayer.play().catch(error => console.log('Background video play error:', error));
  fadeIn('background');
}

function playVideo(videoId) {
  if (currentPlaying) {
    crossFade(currentPlaying, videoId);
  } else {
    videoPlayers[videoId].play().catch(error => console.log('Video play error:', error));
    fadeIn(videoId);
  }
  currentPlaying = videoId;
}

function crossFade(fromId, toId) {
  fadeOut(fromId);
  videoPlayers[toId].play().catch(error => console.log('Video play error:', error));
  fadeIn(toId);
}

function fadeIn(videoId) {
  const gainNode = gainNodes[videoId];
  gainNode.gain.cancelScheduledValues(audioContext.currentTime);
  gainNode.gain.setValueAtTime(0, audioContext.currentTime);
  gainNode.gain.linearRampToValueAtTime(1, audioContext.currentTime + 1); // 1 second fade-in
}

function fadeOut(videoId) {
  const gainNode = gainNodes[videoId];
  gainNode.gain.cancelScheduledValues(audioContext.currentTime);
  gainNode.gain.setValueAtTime(gainNode.gain.value, audioContext.currentTime);
  gainNode.gain.linearRampToValueAtTime(0, audioContext.currentTime + 1); // 1 second fade-out
  setTimeout(() => videoPlayers[videoId].pause(), 1000); // Pause video after fade-out
}

function findVideoByKeytag(database, keytag) {
  const entry = database.find(item => item.keytags.includes(keytag));
  return entry ? entry.id : null;
}
