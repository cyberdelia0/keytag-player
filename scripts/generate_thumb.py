import os
import re
import subprocess
import argparse

def extract_index_and_label(file_name):
    match = re.match(r'(\d+)-(.+)', file_name)
    if match:
        index = match.group(1)
        label = match.group(2)
    else:
        index = 'xx'
        label = file_name
    return index, label

def generate_animated_thumbnails_and_markdown(input_folder):
    # Ensure the folder exists
    if not os.path.exists(input_folder):
        print(f"The folder {input_folder} does not exist.")
        return
    
    video_files = [f for f in os.listdir(input_folder) if f.endswith(('.mp4', '.webm', '.ogg', '.avi', '.mkv'))]
    if not video_files:
        print(f"No video files found in {input_folder}.")
        return

    metadata = []

    for video_file in video_files:
        video_path = os.path.join(input_folder, video_file)
        base_name = os.path.splitext(video_file)[0]
        index, label = extract_index_and_label(base_name)
        thumbnail_file = f"thumb_{base_name}.webp"
        thumbnail_path = os.path.join(input_folder, thumbnail_file)

        # Generate animated thumbnail using ffmpeg
        command = [
            'ffmpeg', '-i', video_path, '-vf', 'fps=10,scale=320:-1:flags=lanczos', '-loop', '0',
            '-y', thumbnail_path
        ]
        subprocess.run(command, check=True)

        # Collect metadata for sorting
        metadata.append((index, label, base_name, thumbnail_file, video_file))

    # Sort metadata by index
    metadata.sort(key=lambda x: x[0])

    # Generate markdown content
    markdown_lines = []

    for index, label, base_name, thumbnail_file, video_file in metadata:
        markdown_lines.append(f'## {index}')
        markdown_lines.append(f'### id')
        markdown_lines.append(f'* {index}')
        markdown_lines.append(f'### label')
        markdown_lines.append(f'* {label}')
        markdown_lines.append(f'### video')
        markdown_lines.append(f'* [![{base_name}]({thumbnail_file})]({video_file})')
        markdown_lines.append('')  # Add an empty line for spacing

    # Write markdown to file
    markdown_file_path = os.path.join(input_folder, 'thumbnails.md')
    with open(markdown_file_path, 'w') as markdown_file:
        markdown_file.write('\n'.join(markdown_lines))

    print(f"Animated thumbnails and markdown file generated in {input_folder}")

def main():
    parser = argparse.ArgumentParser(description="Generate animated thumbnails and a markdown file for video files in a folder.")
    parser.add_argument(
        'input_folder', 
        type=str, 
        help="The folder containing the video files."
    )

    args = parser.parse_args()

    generate_animated_thumbnails_and_markdown(args.input_folder)

if __name__ == "__main__":
    main()
