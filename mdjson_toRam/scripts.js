document.addEventListener("DOMContentLoaded", () => {
    const mediaPath = '../medias/lorem/'; // Define the media path here
    const videoContainer = document.getElementById("video-container");
    const debugContent = document.getElementById("debug-content");
    const thumbnails = document.getElementById("thumbnails");
    const volumeControl = document.getElementById("volume");
    const volumeValue = document.getElementById("volume-value");
    const audioTransitionControl = document.getElementById("audio-transition-time");
    const audioTransitionValue = document.getElementById("audio-transition-value");
    const visualTransitionControl = document.getElementById("visual-transition-time");
    const visualTransitionValue = document.getElementById("visual-transition-value");

    let videoElements = [];
    let currentVideo = null;
    let audioContext;
    let masterGain;
    let audioFadeDuration = 1.0; // Cross-fade duration in seconds
    let visualFadeDuration = 1.0; // Visual fade duration in seconds

    function initializeAudioContext() {
        audioContext = new (window.AudioContext || window.webkitAudioContext)();
        masterGain = audioContext.createGain();
        masterGain.gain.value = 1;
        masterGain.connect(audioContext.destination);
        console.log("AudioContext and MasterGain initialized");
    }

    document.body.addEventListener('click', () => {
        if (!audioContext) {
            initializeAudioContext();
        }
    });

    fetch(`${mediaPath}database.md`)
        .then(response => response.text())
        .then(data => {
            const database = parseMarkdown(data);
            database.forEach((entry, index) => createVideoElements(entry, index));
        });

    volumeControl.addEventListener("input", (e) => {
        if (masterGain) {
            masterGain.gain.value = e.target.value;
            volumeValue.innerText = e.target.value;
            console.log(`Master volume set to ${e.target.value}`);
        }
    });

    audioTransitionControl.addEventListener("input", (e) => {
        audioFadeDuration = parseFloat(e.target.value);
        audioTransitionValue.innerText = `${audioFadeDuration.toFixed(1)}s`;
        console.log(`Audio fade duration set to ${audioFadeDuration} seconds`);
    });

    visualTransitionControl.addEventListener("input", (e) => {
        visualFadeDuration = parseFloat(e.target.value);
        visualTransitionValue.innerText = `${visualFadeDuration.toFixed(1)}s`;
        console.log(`Visual fade duration set to ${visualFadeDuration} seconds`);
    });

    function parseMarkdown(markdown) {
        const lines = markdown.split('\n');
        const database = [];
        let currentEntry = null;

        lines.forEach(line => {
            line = line.trim();

            if (line.startsWith('## ')) {
                if (currentEntry) {
                    database.push(currentEntry);
                }
                currentEntry = { id: null, label: null, caption: null, keytag: null, noteonmidimap: null, videos: [] };
            } else if (line.startsWith('### ')) {
                const key = line.substring(4).toLowerCase();
                currentEntry[key] = null;
            } else if (line.startsWith('* ')) {
                const value = line.substring(2).trim();

                if (currentEntry.id === null || currentEntry.label === null || currentEntry.caption === null || currentEntry.keytag === null || currentEntry.noteonmidimap === null) {
                    const key = Object.keys(currentEntry).find(k => currentEntry[k] === null);
                    currentEntry[key] = value;
                } else {
                    if (!currentEntry.videos) {
                        currentEntry.videos = [];
                    }
                    const match = value.match(/\[!\[(.*?)\]\((.*?)\)\]\((.*?)\)/);
                    if (match) {
                        const [_, text, thumbnail, url] = match;
                        currentEntry.videos.push({ text, thumbnail, url });
                    } else {
                        const fallbackMatch = value.match(/\[(.*?)\]\((.*?)\)/);
                        if (fallbackMatch) {
                            const [_, text, url] = fallbackMatch;
                            currentEntry.videos.push({ text, thumbnail: '', url });
                        } else {
                            currentEntry.videos.push({ text: value, thumbnail: '', url: value });
                        }
                    }
                }
            }
        });

        if (currentEntry) {
            database.push(currentEntry);
        }

        return database;
    }

    function createVideoElements(entry, index) {
        if (!entry.videos || !Array.isArray(entry.videos)) {
            console.error(`Entry ${index} has invalid videos field.`, entry);
            return;
        }

        entry.videos.forEach(videoData => {
            const video = document.createElement("video");
            video.src = `${mediaPath}${videoData.url}`;
            video.controls = false;
            video.autoplay = false;
            video.loop = true; // Loop by default
            video.id = `video_${index}_${videoData.text}`;
            video.volume = 1;
            video.onended = () => stopAllVideos();

            if (!audioContext) {
                initializeAudioContext();
            }

            const track = audioContext.createMediaElementSource(video);
            const gainNode = audioContext.createGain();
            gainNode.gain.value = 0; // Start with muted audio
            track.connect(gainNode).connect(masterGain);
            video.gainNode = gainNode; // Store gain node for later use

            video.fadeIn = function() {
                video.style.transition = `opacity ${visualFadeDuration}s ease`;
                video.style.opacity = 1;
                video.play();
                if (video.gainNode) {
                    video.gainNode.gain.cancelScheduledValues(audioContext.currentTime);
                    video.gainNode.gain.setValueAtTime(0.0001, audioContext.currentTime); // Start from a very low value to avoid abrupt start
                    video.gainNode.gain.linearRampToValueAtTime(0.3, audioContext.currentTime + audioFadeDuration * 0.1); // Quick initial ramp
                    video.gainNode.gain.exponentialRampToValueAtTime(1, audioContext.currentTime + audioFadeDuration); // Smooth ramp to full volume
                    console.log(`Fading in video ${video.id} with gain node value starting from ${video.gainNode.gain.value}`);
                }
            };

            video.fadeOut = function() {
                video.style.transition = `opacity ${visualFadeDuration}s ease`;
                video.style.opacity = 0;
                if (video.gainNode) {
                    video.gainNode.gain.cancelScheduledValues(audioContext.currentTime);
                    video.gainNode.gain.setValueAtTime(video.gainNode.gain.value, audioContext.currentTime); // Ensure it starts from current value
                    video.gainNode.gain.linearRampToValueAtTime(0.3, audioContext.currentTime + audioFadeDuration * 0.1); // Quick initial ramp
                    video.gainNode.gain.exponentialRampToValueAtTime(0.0001, audioContext.currentTime + audioFadeDuration); // Smooth ramp to very low volume
                    console.log(`Fading out video ${video.id} with gain node value starting from ${video.gainNode.gain.value}`);
                }
                video.addEventListener('transitionend', function handler() {
                    video.pause();
                    video.removeEventListener('transitionend', handler);
                });
            };

            videoContainer.appendChild(video);
            videoElements.push(video);

            const thumbnailContainer = document.createElement("div");
            thumbnailContainer.className = "thumbnail-container";

            const thumbnail = document.createElement("img");
            thumbnail.src = `${mediaPath}${videoData.thumbnail}`;
            thumbnail.alt = videoData.text;
            thumbnail.onclick = () => playVideo(video);

            const description = document.createElement("p");
            description.innerText = videoData.text;
            description.onclick = () => playVideo(video);

            thumbnailContainer.appendChild(thumbnail);
            thumbnailContainer.appendChild(description);
            thumbnails.appendChild(thumbnailContainer);
        });
    }

    function playVideo(video) {
        if (currentVideo && currentVideo !== video) {
            const previousVideo = currentVideo;
            previousVideo.fadeOut();
        }

        video.fadeIn();
        currentVideo = video;
        console.log(`Playing video ${video.id}`);
    }

    function stopAllVideos() {
        videoElements.forEach(video => {
            video.pause();
            video.style.opacity = 0;
            if (video.gainNode) {
                video.gainNode.gain.value = 0;
            }
        });
        console.log("Stopped all videos");
    }

    window.toggleDebugMenu = function () {
        debugContent.style.display = debugContent.style.display === "block" ? "none" : "block";
    };

    window.toggleThumbnails = function () {
        const thumbnails = document.getElementById("thumbnails");
        const thumbnailToggle = document.getElementById("thumbnail-toggle");
        if (thumbnails.classList.contains("hidden")) {
            thumbnails.classList.remove("hidden");
            thumbnails.style.maxHeight = "50vh"; // Set the max height when shown
            thumbnailToggle.innerText = "Hide Thumbnails";
        } else {
            thumbnails.classList.add("hidden");
            thumbnails.style.maxHeight = "0"; // Set the max height to 0 when hidden
            thumbnailToggle.innerText = "Show Thumbnails";
        }
    };
});
