## 1
### id
* 1
### label
* Tag 00001
### caption
* This is the caption for Tag 00001
### videos
* [![ipsum_001](./thumb_01-sentience.webp)](./01-sentience.mp4)
### keytag
* 0000000001
### NoteOnMidiMap
* 36

## 2
### id
* 2
### label
* Tag 00002
### caption
* This is the caption for Tag 00002
### videos
* [![ipsum_009](./thumb_ipsum_009.webp)](./ipsum_009.mp4)
* [![ipsum_010](./thumb_ipsum_010.webp)](./ipsum_010.mp4)
* [![ipsum_011](./thumb_ipsum_011.webp)](./ipsum_011.mp4)
* [![ipsum_012](./thumb_ipsum_012.webp)](./ipsum_012.mp4)
* [![ipsum_013](./thumb_ipsum_013.webp)](./ipsum_013.mp4)
* [![ipsum_014](./thumb_ipsum_014.webp)](./ipsum_014.mp4)
* [![ipsum_015](./thumb_ipsum_015.webp)](./ipsum_015.mp4)
* [![ipsum_016](./thumb_ipsum_016.webp)](./ipsum_016.mp4)
### keytag
* 0000000002
### NoteOnMidiMap
* 37

## 3
### id
* 3
### label
* Tag 00003
### caption
* This is the caption for Tag 00003
### videos
* [![ipsum_017](./thumb_ipsum_017.webp)](./ipsum_017.mp4)
* [![ipsum_018](./thumb_ipsum_018.webp)](./ipsum_018.mp4)
* [![ipsum_019](./thumb_ipsum_019.webp)](./ipsum_019.mp4)
* [![ipsum_020](./thumb_ipsum_020.webp)](./ipsum_020.mp4)
* [![ipsum_021](./thumb_ipsum_021.webp)](./ipsum_021.mp4)
* [![ipsum_022](./thumb_ipsum_022.webp)](./ipsum_022.mp4)
* [![ipsum_023](./thumb_ipsum_023.webp)](./ipsum_023.mp4)
* [![ipsum_024](./thumb_ipsum_024.webp)](./ipsum_024.mp4)
### keytag
* 0000000003
### NoteOnMidiMap
* 38

## 4
### id
* 4
### label
* Tag 00004
### caption
* This is the caption for Tag 00004
### videos
* [![ipsum_025](./thumb_ipsum_025.webp)](./ipsum_025.mp4)
* [![ipsum_026](./thumb_ipsum_026.webp)](./ipsum_026.mp4)
* [![ipsum_027](./thumb_ipsum_027.webp)](./ipsum_027.mp4)
* [![ipsum_028](./thumb_ipsum_028.webp)](./ipsum_028.mp4)
* [![ipsum_029](./thumb_ipsum_029.webp)](./ipsum_029.mp4)
* [![ipsum_030](./thumb_ipsum_030.webp)](./ipsum_030.mp4)
* [![ipsum_031](./thumb_ipsum_031.webp)](./ipsum_031.mp4)
* [![ipsum_032](./thumb_ipsum_032.webp)](./ipsum_032.mp4)
### keytag
* 0000000004
### NoteOnMidiMap
* 39

## 5
### id
* 5
### label
* Tag 00005
### caption
* This is the caption for Tag 00005
### videos
* [![ipsum_033](./thumb_ipsum_033.webp)](./ipsum_033.mp4)
* [![ipsum_034](./thumb_ipsum_034.webp)](./ipsum_034.mp4)
* [![ipsum_035](./thumb_ipsum_035.webp)](./ipsum_035.mp4)
* [![ipsum_036](./thumb_ipsum_036.webp)](./ipsum_036.mp4)
* [![ipsum_037](./thumb_ipsum_037.webp)](./ipsum_037.mp4)
* [![ipsum_038](./thumb_ipsum_038.webp)](./ipsum_038.mp4)
* [![ipsum_039](./thumb_ipsum_039.webp)](./ipsum_039.mp4)
* [![ipsum_040](./thumb_ipsum_040.webp)](./ipsum_040.mp4)
### keytag
* 0000000005
### NoteOnMidiMap
* 40

## 6
### id
* 6
### label
* Tag 00006
### caption
* This is the caption for Tag 00006
### videos
* [![ipsum_041](./thumb_ipsum_041.webp)](./ipsum_041.mp4)
* [![ipsum_042](./thumb_ipsum_042.webp)](./ipsum_042.mp4)
* [![ipsum_043](./thumb_ipsum_043.webp)](./ipsum_043.mp4)
* [![ipsum_044](./thumb_ipsum_044.webp)](./ipsum_044.mp4)
* [![ipsum_045](./thumb_ipsum_045.webp)](./ipsum_045.mp4)
* [![ipsum_046](./thumb_ipsum_046.webp)](./ipsum_046.mp4)
* [![ipsum_047](./thumb_ipsum_047.webp)](./ipsum_047.mp4)
* [![ipsum_048](./thumb_ipsum_048.webp)](./ipsum_048.mp4)
### keytag
* 0000000006
### NoteOnMidiMap
* 41

## 7
### id
* 7
### label
* Tag 00007
### caption
* This is the caption for Tag 00007
### videos
* [![ipsum_049](./thumb_ipsum_049.webp)](./ipsum_049.mp4)
* [![ipsum_050](./thumb_ipsum_050.webp)](./ipsum_050.mp4)
* [![ipsum_051](./thumb_ipsum_051.webp)](./ipsum_051.mp4)
* [![ipsum_052](./thumb_ipsum_052.webp)](./ipsum_052.mp4)
* [![ipsum_053](./thumb_ipsum_053.webp)](./ipsum_053.mp4)
* [![ipsum_054](./thumb_ipsum_054.webp)](./ipsum_054.mp4)
* [![ipsum_055](./thumb_ipsum_055.webp)](./ipsum_055.mp4)
* [![ipsum_056](./thumb_ipsum_056.webp)](./ipsum_056.mp4)
### keytag
* 0000000007
### NoteOnMidiMap
* 42

## 8
### id
* 8
### label
* Tag 00008
### caption
* This is the caption for Tag 00008
### videos
* [![ipsum_057](./thumb_ipsum_057.webp)](./ipsum_057.mp4)
* [![ipsum_058](./thumb_ipsum_058.webp)](./ipsum_058.mp4)
* [![ipsum_059](./thumb_ipsum_059.webp)](./ipsum_059.mp4)
* [![ipsum_060](./thumb_ipsum_060.webp)](./ipsum_060.mp4)
* [![ipsum_061](./thumb_ipsum_061.webp)](./ipsum_061.mp4)
* [![ipsum_062](./thumb_ipsum_062.webp)](./ipsum_062.mp4)
* [![ipsum_063](./thumb_ipsum_063.webp)](./ipsum_063.mp4)
* [![ipsum_064](./thumb_ipsum_064.webp)](./ipsum_064.mp4)
### keytag
* 0000000008
### NoteOnMidiMap
* 43

## 9
### id
* 9
### label
* Tag 00009
### caption
* This is the caption for Tag 00009
### videos
* [![ipsum_065](./thumb_ipsum_065.webp)](./ipsum_065.mp4)
* [![ipsum_066](./thumb_ipsum_066.webp)](./ipsum_066.mp4)
* [![ipsum_067](./thumb_ipsum_067.webp)](./ipsum_067.mp4)
* [![ipsum_068](./thumb_ipsum_068.webp)](./ipsum_068.mp4)
* [![ipsum_069](./thumb_ipsum_069.webp)](./ipsum_069.mp4)
* [![ipsum_070](./thumb_ipsum_070.webp)](./ipsum_070.mp4)
* [![ipsum_071](./thumb_ipsum_071.webp)](./ipsum_071.mp4)
* [![ipsum_072](./thumb_ipsum_072.webp)](./ipsum_072.mp4)
### keytag
* 0000000009
### NoteOnMidiMap
* 44

## 10
### id
* 10
### label
* Tag 00010
### caption
* This is the caption for Tag 00010
### videos
* [![ipsum_073](./thumb_ipsum_073.webp)](./ipsum_073.mp4)
* [![ipsum_074](./thumb_ipsum_074.webp)](./ipsum_074.mp4)
* [![ipsum_075](./thumb_ipsum_075.webp)](./ipsum_075.mp4)
* [![ipsum_076](./thumb_ipsum_076.webp)](./ipsum_076.mp4)
* [![ipsum_077](./thumb_ipsum_077.webp)](./ipsum_077.mp4)
* [![ipsum_078](./thumb_ipsum_078.webp)](./ipsum_078.mp4)
* [![ipsum_079](./thumb_ipsum_079.webp)](./ipsum_079.mp4)
* [![ipsum_080](./thumb_ipsum_080.webp)](./ipsum_080.mp4)
### keytag
* 0000000010
### NoteOnMidiMap
* 45

## 11
### id
* 11
### label
* Tag 00011
### caption
* This is the caption for Tag 00011
### videos
* [![ipsum_081](./thumb_ipsum_081.webp)](./ipsum_081.mp4)
* [![ipsum_082](./thumb_ipsum_082.webp)](./ipsum_082.mp4)
* [![ipsum_083](./thumb_ipsum_083.webp)](./ipsum_083.mp4)
* [![ipsum_084](./thumb_ipsum_084.webp)](./ipsum_084.mp4)
* [![ipsum_085](./thumb_ipsum_085.webp)](./ipsum_085.mp4)
* [![ipsum_086](./thumb_ipsum_086.webp)](./ipsum_086.mp4)
* [![ipsum_087](./thumb_ipsum_087.webp)](./ipsum_087.mp4)
* [![ipsum_088](./thumb_ipsum_088.webp)](./ipsum_088.mp4)
### keytag
* 0000000011
### NoteOnMidiMap
* 46

## 12
### id
* 12
### label
* Tag 00012
### caption
* This is the caption for Tag 00012
### videos
* [![ipsum_089](./thumb_ipsum_089.webp)](./ipsum_089.mp4)
* [![ipsum_090](./thumb_ipsum_090.webp)](./ipsum_090.mp4)
* [![ipsum_091](./thumb_ipsum_091.webp)](./ipsum_091.mp4)
* [![ipsum_092](./thumb_ipsum_092.webp)](./ipsum_092.mp4)
* [![ipsum_093](./thumb_ipsum_093.webp)](./ipsum_093.mp4)
* [![ipsum_094](./thumb_ipsum_094.webp)](./ipsum_094.mp4)
* [![ipsum_095](./thumb_ipsum_095.webp)](./ipsum_095.mp4)
* [![ipsum_096](./thumb_ipsum_096.webp)](./ipsum_096.mp4)
### keytag
* 0000000012
### NoteOnMidiMap
* 47

## 13
### id
* 13
### label
* Tag 00013
### caption
* This is the caption for Tag 00013
### videos
* [![ipsum_097](./thumb_ipsum_097.webp)](./ipsum_097.mp4)
* [![ipsum_098](./thumb_ipsum_098.webp)](./ipsum_098.mp4)
* [![ipsum_099](./thumb_ipsum_099.webp)](./ipsum_099.mp4)
* [![ipsum_100](./thumb_ipsum_100.webp)](./ipsum_100.mp4)
* [![ipsum_101](./thumb_ipsum_101.webp)](./ipsum_101.mp4)
* [![ipsum_102](./thumb_ipsum_102.webp)](./ipsum_102.mp4)
* [![ipsum_103](./thumb_ipsum_103.webp)](./ipsum_103.mp4)
* [![ipsum_104](./thumb_ipsum_104.webp)](./ipsum_104.mp4)
### keytag
* 0000000013
### NoteOnMidiMap
* 48

## 14
### id
* 14
### label
* Tag 00014
### caption
* This is the caption for Tag 00014
### videos
* [![ipsum_105](./thumb_ipsum_105.webp)](./ipsum_105.mp4)
* [![ipsum_106](./thumb_ipsum_106.webp)](./ipsum_106.mp4)
* [![ipsum_107](./thumb_ipsum_107.webp)](./ipsum_107.mp4)
* [![ipsum_108](./thumb_ipsum_108.webp)](./ipsum_108.mp4)
* [![ipsum_109](./thumb_ipsum_109.webp)](./ipsum_109.mp4)
* [![ipsum_110](./thumb_ipsum_110.webp)](./ipsum_110.mp4)
* [![ipsum_111](./thumb_ipsum_111.webp)](./ipsum_111.mp4)
* [![ipsum_112](./thumb_ipsum_112.webp)](./ipsum_112.mp4)
### keytag
* 0000000014
### NoteOnMidiMap
* 49

## 15
### id
* 15
### label
* Tag 00015
### caption
* This is the caption for Tag 00015
### videos
* [![ipsum_113](./thumb_ipsum_113.webp)](./ipsum_113.mp4)
* [![ipsum_114](./thumb_ipsum_114.webp)](./ipsum_114.mp4)
* [![ipsum_115](./thumb_ipsum_115.webp)](./ipsum_115.mp4)
* [![ipsum_116](./thumb_ipsum_116.webp)](./ipsum_116.mp4)
* [![ipsum_117](./thumb_ipsum_117.webp)](./ipsum_117.mp4)
* [![ipsum_118](./thumb_ipsum_118.webp)](./ipsum_118.mp4)
* [![ipsum_119](./thumb_ipsum_119.webp)](./ipsum_119.mp4)
* [![ipsum_120](./thumb_ipsum_120.webp)](./ipsum_120.mp4)
### keytag
* 0000000015
### NoteOnMidiMap
* 50

## 16
### id
* 16
### label
* Tag 00016
### caption
* This is the caption for Tag 00016
### videos
* [![ipsum_121](./thumb_ipsum_121.webp)](./ipsum_121.mp4)
* [![ipsum_122](./thumb_ipsum_122.webp)](./ipsum_122.mp4)
* [![ipsum_123](./thumb_ipsum_123.webp)](./ipsum_123.mp4)
* [![ipsum_124](./thumb_ipsum_124.webp)](./ipsum_124.mp4)
* [![ipsum_125](./thumb_ipsum_125.webp)](./ipsum_125.mp4)
* [![ipsum_126](./thumb_ipsum_126.webp)](./ipsum_126.mp4)
* [![ipsum_127](./thumb_ipsum_127.webp)](./ipsum_127.mp4)
* [![ipsum_128](./thumb_ipsum_128.webp)](./ipsum_128.mp4)
### keytag
* 0000000016
### NoteOnMidiMap
* 51
